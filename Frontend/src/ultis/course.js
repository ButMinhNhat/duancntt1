export const isLecture = (o = {}) => {
    return o.hasOwnProperty('files');
}

export const isQuiz = (o = {}) => {
    return o.hasOwnProperty('questions');
}