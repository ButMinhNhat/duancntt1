import { IconButton, Typography } from "@mui/material";
import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import axios from "axios";

const AttachFile = (props) => {
  const { file } = props;

  const removeFile = async () => {
    try {
      axios({
        method: "delete",
        url: `${process.env.REACT_APP_API_URL}/file/${file}`,
        headers: { "Content-Type": "application/json" },
      })
        .then(function (res) {
          props.handleRemove(file);
        })
        .catch(function (err) {
          console.error(err);
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="flex items-center bg-gray-200 rounded-md p-2 max-w-[400px] my-2">
      <Typography variant="caption" className="flex-1 truncate text-md font-bold">
        {file}
      </Typography>
      <IconButton onClick={removeFile}>
        <CloseIcon />
      </IconButton>
    </div>
  );
};

export default AttachFile;
