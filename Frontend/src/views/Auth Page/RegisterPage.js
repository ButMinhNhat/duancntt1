import React, { useState } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import { TextField } from "@mui/material";
import CustomButton from "components/CustomButton";
import { useDispatch } from "react-redux";
import * as Actions from "actions";
import { useNavigate } from "react-router-dom";
import * as mutations from "graphql/mutation";

const RegisterPage = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();
  
	const [data, setData] = useState({
	  username: "",
	  password: "",
	  name: ""
	});
  
	const handleChange = (e) => {
	  setData({
		...data,
		[e.target.name]: e.target.value,
	  });
	};
  
	const handleSubmit = async (e) => {
	  e.preventDefault();
	  if (data.username && data.password) {
		try {
		  const result = await mutations.register(data);
		  if(result) {
			  dispatch(Actions.loginAccount(result));
			  navigate("/");
		  }
		} catch (error) {
		  console.log(error);
		}
	  }
	};
  
	return (
	  <HomeLayout>
		<form className="max-w-[400px] mx-auto" onSubmit={handleSubmit}>
		  <div className="font-semibold text-xl text-center">
			Register and start learning
		  </div>
		  <div className="border-t-2 my-4 border-slate-300" />
		  <TextField
		  	autoComplete="off"
			className="my-4"
			placeholder="Fullname"
			name="name"
			variant="outlined"
			value={data.name}
			inputProps={{
			  style: {
				padding: 12,
			  },
			}}
			onChange={handleChange}
			fullWidth
			required
		  />
		  <TextField
		  	autoComplete="off"
			className="my-4"
			placeholder="Username"
			name="username"
			variant="outlined"
			value={data.username}
			inputProps={{
			  style: {
				padding: 12,
			  },
			}}
			onChange={handleChange}
			fullWidth
			required
		  />
		  <TextField
		  	autoComplete="off"
			className="my-4"
			placeholder="Password"
			name="password"
			type="password"
			variant="outlined"
			value={data.password}
			inputProps={{
			  style: {
				padding: 12,
			  },
			}}
			onChange={handleChange}
			fullWidth
			required
		  />
		  <div className="border-t-2 my-4 border-slate-300" />
		  <CustomButton
		  	type="submit"
			className="
				  w-full px-4 py-2 font-semibold text-center text-xl 
				  mt-4 bg-violet-600 text-white hover:bg-violet-700 
				  disabled:bg-stone-300 disabled:text-black
			  "
			disabled={!data.username || !data.password || !data.name}
		  >
			Register
		  </CustomButton>
		  <div className="mt-8 text-center">
		    Already have an account ? {" "}
			<a
			  href="/auth/signup"
			  className="font-semibold text-violet-700 underline"
			>
			  To log in
			</a>
		  </div>
		</form>
	  </HomeLayout>
	);
}

export default RegisterPage;