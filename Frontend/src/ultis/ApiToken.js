export const getApiToken = () => {
	const token = localStorage.getItem('token');
	return token;
}

export const saveApiToken = (token) => {
	localStorage.setItem('token', token);
}