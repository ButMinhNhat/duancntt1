import React, { useEffect } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import { useDispatch, useSelector } from "react-redux";
import * as Actions from "actions";
import { courseDefaultThumbnail } from "components/CourseInformation";
import { Link } from "react-router-dom";
import CustomButton from "components/CustomButton";
import { IconButton, Tooltip } from "@mui/material";
import Edit from "@mui/icons-material/Edit";

const InstructorPage = () => {
  const dispatch = useDispatch();

  const courses = useSelector(({ courses }) => courses.data);

  useEffect(() => {
    dispatch(Actions.getCoursesInstructor());

	return () => {
		dispatch(Actions.clearCoursesListData());
	}
  }, [dispatch]);

  if (!courses) return <HomeLayout />;

  return (
    <HomeLayout>
	  <div className="flex pb-4 mb-4 items-center border-b-[1px] border-slate-300">
		<div className="text-3xl font-bold flex-1">My Instructor</div>
		<Link to="/instructor/new">
			<CustomButton>Create New</CustomButton>
		</Link>
	  </div>
      <div className="grid grid-cols-4 gap-10">
        {courses.map((item, index) => (
          <div
            className="w-full bg-white border-[1px] border-slate-300 relative"
            key={index}
          >
			<Link to={`/instructor/${item.slug}`} className="absolute top-2 left-2">
				<Tooltip title="Edit course">
					<IconButton className="bg-white hover:bg-slate-200"><Edit /></IconButton>
				</Tooltip>
			</Link>
            <Link to={`/course/${item.slug}`}>
              <img
                className="w-full h-[180px]"
                src={
                  item.coverImage
                    ? `${process.env.REACT_APP_API_URL}/file/${item.coverImage}`
                    : courseDefaultThumbnail
                }
                alt="course thumbnail"
              />
            </Link>
            <div className="p-2">
              <Link
                to={`/course/${item.slug}`}
                className="my-2 font-bold text-xl line-clamp-2"
              >
                {item.title}
              </Link>
            </div>
          </div>
        ))}
      </div>
    </HomeLayout>
  );
};

export default InstructorPage;
