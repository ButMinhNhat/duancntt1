import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import CourseDetailPage from "views/Course Page/CourseDetailPage";
import CourseListPage from "views/Course Page/CourseListPage";
import NotFound from "layouts/NotFound";
import HomePage from "views/Home Page/HomePage";
import InstructorPage from "views/Instructor/InstructorPage";
import CourseEdit from "views/Instructor/CourseEdit";
import LoginPage from "views/Auth Page/LoginPage";
import RegisterPage from "views/Auth Page/RegisterPage";
import CourseLearnPage from "views/Course Page/CourseLearnPage";
import MyLearningPage from "views/My Learning/MyLearningPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/courses" element={<CourseListPage />} />
        <Route path="/course/:slug" element={<CourseDetailPage />} />
		<Route path="/learn/:slug" element={<CourseLearnPage />} />
		<Route path="/my-learning" element={<MyLearningPage />} />
		<Route path="/instructor">
			<Route path="/instructor" element={<InstructorPage />} />
			<Route path="/instructor/:slug" element={<CourseEdit />} />
		</Route>
		<Route path="/auth">
			<Route path="/auth" element={<Navigate to="/auth/signin"/>} />
			<Route path="/auth/signin" element={<LoginPage />} />
			<Route path="/auth/signup" element={<RegisterPage />} />
		</Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
