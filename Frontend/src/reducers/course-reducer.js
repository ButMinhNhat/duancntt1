import { createSlice } from "@reduxjs/toolkit";
import * as Actions from "actions";

const courseSlice = createSlice({
    name: 'course',
    initialState:{
		data: null,
		transactions: null,
		tracking: null
	},
    reducers:{},
	extraReducers: (builder) => {
		builder.addCase(Actions.createNewCourse.fulfilled, (state, action) => {
		    state.data = action.payload
		});
		builder.addCase(Actions.getCourseBySlug.fulfilled, (state, action) => {
			state.data = action.payload
		});
		builder.addCase(Actions.getCoursesInstructorBySlug.fulfilled, (state, action) => {
			state.data = action.payload
		});
		builder.addCase(Actions.clearCourseData, (state) => {
			state.data = null;
			state.transactions = null;
			state.tracking = null;
		});
		builder.addCase(Actions.getCourseTransactions.fulfilled, (state, action) => {
			state.transactions = action.payload
		});
		builder.addCase(Actions.getCourseLearnTracking.fulfilled, (state, action) => {
			state.tracking = action.payload
		});
		builder.addCase(Actions.updateCourseLearnTracking.fulfilled, (state, action) => {
			state.tracking = action.payload
		});
	},	
});

const { reducer } = courseSlice;
export default reducer;