import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import * as queries from 'graphql/query';
import * as mutations from 'graphql/mutation';
import { trackingEvent } from "ultis/Common";

export const createNewCourse = createAsyncThunk(
	'GET New course',
	async (data) => {
		return data;
	}
)

export const getCourseBySlug = createAsyncThunk(
	'GET Course by slug',
	async (slug) => {
		const result = await queries.getCourseBySlug(slug);
		if(result.data) {
			return result.data
		}
	}
)

export const getCoursesInstructorBySlug = createAsyncThunk(
	'GET Course Intructor by slug',
	async (slug) => {
		const { data } = await queries.getInstructorCourseBySlug(slug);
		if(data) {
			return data;
		}
	}
)

export const clearCourseData = createAction('Clear course data');

export const getCourseTransactions = createAsyncThunk(
	'GET Course Transactions',
	async ({types = [], courseId}) => {
		const { data } = await queries.getTransactionByTypes(types, courseId);
		if(data) {
			return data;
		}
	}
)

export const getCourseLearnTracking = createAsyncThunk(
	'GET Course Learn Tracking',
	async ({ type, courseId }) => {
		const { data, error } = await queries.queryTracking(type, courseId)
		if(error) console.log(error);
		if(data) {
			return data
		} else {
			const { data, error } = await mutations.saveTracking({
				type: trackingEvent.COURSE_LEARN,
				course: courseId,
				data: []
			})
			if (error) console.log(error);
			if (data) {
				return data
			}
		}
	}
)

export const updateCourseLearnTracking = createAsyncThunk(
	'Update Course Learn Tracking',
	async (data) => data
)