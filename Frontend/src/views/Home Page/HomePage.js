import React from "react";
import HomeLayout from "layouts/Home/HomeLayout";

const HomePage = () => {
  return (
    <HomeLayout>
      <div>HomePage</div>
    </HomeLayout>
  );
};

export default HomePage;
