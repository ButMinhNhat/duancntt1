import { TextField } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Rate from "rc-rate";
import React, { useState } from "react";
import CustomButton from "./CustomButton";
import "rc-rate/assets/index.css";
import { transactionEvent } from "ultis/Common";
import { useDispatch, useSelector } from "react-redux";
import * as mutations from "graphql/mutation";
import * as Actions from 'actions';

const useStyles = makeStyles((theme) => ({
  rateWrap: {
    "& .rc-rate": {
      fontSize: 60,
    },
  },
  container: {
    minHeight: 320,
    width: "100%",
    padding: 32,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  title: {
    fontWeight: "bold",
    fontSize: 30,
  },
  input: {
    marginTop: 20,
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "row-reverse",
    marginTop: 30,
    width: "100%",
  },
  saveButton: {
    backgroundColor: "black",
    color: "white",
    padding: 12,
    fontWeight: "bold",
    "&:hover": {
      opacity: 0.9,
    },
  },
}));

const RatingBox = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { COURSE_COMMENT, COURSE_RATING } = transactionEvent;

  const [star, setStar] = useState(0);
  const [input, setInput] = useState("");
  const course = useSelector(({ course }) => course.data);

  const handleSaveRating = async () => {
    try {
      const inputProp = {
        type: COURSE_RATING,
        course: course._id,
        content: {
          text: input,
          value: star,
        },
      };
	  const { data, error } = await mutations.createTransaction(inputProp);
      if (error) return console.log(error);
      if (data) {
        props.onClose();
        return dispatch(
          Actions.getCourseTransactions({
            types: [COURSE_COMMENT, COURSE_RATING],
            courseId: course._id,
          })
        );
      }
	  props.onClose();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className={classes.container}>
      <div className={classes.title}>How would you rate this course?</div>
      <div className={classes.rateWrap}>
        <Rate
          value={star}
          onChange={(value) => setStar(value)}
          count={5}
          allowHalf
        />
      </div>
      <TextField
        fullWidth
        value={input}
        multiline
        rows={4}
        InputProps={{
          className: classes.input,
        }}
        inputProps={{ spellCheck: "false" }}
        onChange={(e) => setInput(e.target.value)}
        placeholder="Tell us about your experience with this course.  Did it meet your expectations?"
      />
      <div className={classes.buttonContainer}>
        {!!star && (
          <CustomButton
            className={classes.saveButton}
            onClick={() => handleSaveRating()}
          >
            Save and continue
          </CustomButton>
        )}
      </div>
    </div>
  );
};

export default RatingBox;
