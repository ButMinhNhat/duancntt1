import {
  Collapse,
  Divider,
  IconButton,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { makeStyles } from "@mui/styles";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import CustomButton from "./CustomButton";
import PlayCircleIcon from "@mui/icons-material/PlayCircle";
import DescriptionIcon from "@mui/icons-material/Description";
import CKEditorInput from "./CKEditorInput";
import CustomUpload from "./CustomUpload";
import AttachFile from "./AttachFile";

const useStyles = makeStyles((theme) => ({
  hiddenContent: {
    "& .hide": {
      opacity: 0,
      visibility: "hidden",
      //   transition: theme.transitions.create("all", {
      //     duration: theme.transitions.duration.complex,
      //     easing: theme.transitions.easing.easeInOut,
      //   }),
    },
    "&:hover": {
      "& .hide": {
        opacity: 1,
        visibility: "visible",
      },
    },
  },
}));

export const lectureTypes = [
  {
    name: "Video",
    icon: <PlayCircleIcon />,
  },
  {
    name: "Article",
    icon: <DescriptionIcon />,
  },
];
// const contentTypes = ["CONTENT_TEXT", "CONTENT_FILE"];
// const acceptVideoType = "video/mp4,video/x-m4v,video/*";
// const acceptDocsType = "application/pdf";

const CourseLecture = (props) => {
  const { lecture, idx, handleChange } = props;
  const classes = useStyles();
  const titleInputRef = useRef(null);

  const [isChangeTitle, setIsChangeTitle] = useState(!lecture.title);
  const [isExpand, setIsExpand] = useState(false);
  const [lectureType, setLectureType] = useState("");
  const [lectureContent, setLectureContent] = useState("");

  const handleDeleteLecture = () => {
    handleChange(idx, null);
  };

  const handleChangeLectureTitle = () => {
    if (!titleInputRef?.current) return;
    const { value } = titleInputRef?.current;
    handleChange(idx, {
      ...lecture,
      title: value,
    });
    setIsChangeTitle(false);
  };

  const handleChangeLectureType = (value) => {
    if (value === lectureTypes[1].name) {
      handleChange(idx, {
        ...lecture,
        content: "",
        type: value,
        expand: true,
      });
    }

    if (value === lectureTypes[0].name) {
      handleChange(idx, {
        ...lecture,
        type: value,
        expand: true,
      });
    }
    setLectureType(value);
  };

  const changeLectureContent = (content) => {
    setLectureContent(content);
  };

  const saveLectureContent = () => {
    handleChange(idx, {
      ...lecture,
      content: lectureContent,
      expand: false,
    });
  };

  const handleUploadLectureVideo = (file) => {
    handleChange(idx, {
      ...lecture,
      media: file,
    });
  };

  const handleRemoveVideo = () => {
    handleChange(idx, {
      ...lecture,
      media: "",
    });
  };

  const handleUploadLectureResources = (file) => {
    handleChange(idx, {
      ...lecture,
      files: [...lecture.files, file],
    });
  };

  const handleRemoveResource = (file) => {
    handleChange(idx, {
      ...lecture,
      files: lecture.files.filter((f) => f !== file),
    });
  };

  useEffect(() => {
    setLectureType(lecture.type);
  }, [lecture.type]);

  useEffect(() => {
    if (typeof lecture.expand === "boolean") {
      setIsExpand(lecture.expand);
    } else {
      setIsExpand(false);
    }
  }, [lecture.expand]);

  useEffect(() => {
    setLectureContent(lecture.content);
  }, [lecture.content]);

  useEffect(() => {
    //update lecture, check lecture type
    if (!lectureType) {
      if (lecture.media) {
        setLectureType(lectureTypes[0].name);
      }
      if (lecture.content || lecture.files?.length > 0) {
        //check if "content" field is url => lecture type is link
        setLectureType(lectureTypes[1].name);
      }
    }
  }, [lecture, lectureType]);

  return (
    <div className="border-1 p-2 my-2">
      <div className="flex items-center">
        <div className={`${classes.hiddenContent} flex-1 flex items-center `}>
          <Typography className="font-bold text-md min-w-[120px]">
            {lecture.title}
          </Typography>
          <div className="flex-1 flex items-center">
            <div className="flex items-center ml-24">
              <Tooltip title="Edit title" placement="top" className="mx-2">
                <IconButton
                  onClick={() => {
                    setIsChangeTitle(true);
                    setIsExpand(true);
                  }}
                >
                  <EditIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Delete section" placement="top" className="mx-2">
                <IconButton onClick={handleDeleteLecture}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
          {lectureType && (
            <div>
              <IconButton onClick={() => setIsExpand(!isExpand)}>
                {isExpand ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
              </IconButton>
            </div>
          )}
        </div>
      </div>
      {isChangeTitle && isExpand && (
        <div className="my-4 bg-white p-4 border-[1px] border-slate-700 rounded-sm">
          <TextField
		    autoComplete="off"
            placeholder="Section title"
            name="title"
            variant="outlined"
            inputRef={titleInputRef}
            defaultValue={lecture.title}
            fullWidth
            required
            autoFocus
          />
          <div className="my-4 flex justify-end">
            <CustomButton
              className="mr-8"
              variant="contained"
              onClick={() => setIsChangeTitle(false)}
            >
              Cancel
            </CustomButton>
            <CustomButton
              variant="contained"
              color="primary"
              onClick={handleChangeLectureTitle}
            >
              Save
            </CustomButton>
          </div>
        </div>
      )}
      {(!isExpand || !lectureType) && (
        <>
          <Divider />
          {lectureType ? (
            <div className="mt-4">
              {lectureTypes.find((item) => item.name === lectureType)?.icon}
            </div>
          ) : (
            <div className="flex w-full sm:items-center mt-4 flex-col sm:flex-row">
              {lectureTypes.map((item, index) => (
                <CustomButton
                  className="flex items-center cursor-pointer bg-white px-4 py-2 border-[1px] bg-gray-200 text-black hover:bg-gray-300 mr-4"
                  onClick={() => handleChangeLectureType(item.name)}
                  key={index}
                >
                  {item.icon}
                  <Typography className="ml-4">Add {item.name}</Typography>
                </CustomButton>
              ))}
            </div>
          )}
        </>
      )}
      <Collapse in={isExpand && lectureType}>
        <div className="border-t-1 py-4">
          {/* change to another lecture type options */}
          <div className="flex w-full mb-4 justify-end">
            {lectureTypes
              .filter((item) => item.name !== lectureType)
              .map((item, index) => (
                <CustomButton
                  className="flex items-center cursor-pointer bg-white px-4 py-2 border-[1px] bg-gray-200 text-black hover:bg-gray-300"
                  onClick={() => handleChangeLectureType(item.name)}
                  key={index}
                >
                  {item.icon}
                  <Typography className="ml-4">
                    Change to {item.name}
                  </Typography>
                </CustomButton>
              ))}
          </div>

          {/* article content */}
          {lectureType === lectureTypes[1].name && (
            <CKEditorInput
              style={{ "--content-height": 120 }}
              name="content"
              value={lectureContent}
              onChange={changeLectureContent}
            />
          )}

          {/* video content */}
          {lectureType === lectureTypes[0].name && (
            <div>
              {lecture.media && (
                <div className="my-2">
                  <Typography className="font-bold">- Video:</Typography>
                  <AttachFile
                    file={lecture.media}
                    handleRemove={handleRemoveVideo}
                  />
                </div>
              )}
              <CustomUpload
                handleUpload={handleUploadLectureVideo}
                accept="video/mp4,video/x-m4v,video/*"
              >
                <CustomButton
                  className="block normal-case"
                  color="primary"
                  variant="contained"
                >
                  {lecture.media ? "Replace video" : "Add video"}
                </CustomButton>
              </CustomUpload>
            </div>
          )}

          {/* resources */}
          <div className="my-8">
            <CustomUpload
              handleUpload={handleUploadLectureResources}
              accept="application/pdf"
            >
              <CustomButton
                className="block normal-case"
                variant="contained"
                color="primary"
              >
                Add resources
              </CustomButton>
            </CustomUpload>
            {lecture.files && lecture.files.length > 0 && (
              <div className="my-8">
                <Typography className="font-bold">- Resources :</Typography>
                {lecture.files.map((item, index) => (
                  <AttachFile
                    key={index}
                    file={item}
                    handleRemove={handleRemoveResource}
                  />
                ))}
              </div>
            )}
          </div>

          {lectureType === lectureTypes[1].name && (
            <CustomButton
              className="mt-4 block normal-case ml-auto"
              onClick={saveLectureContent}
            >
              Save content
            </CustomButton>
          )}
        </div>
      </Collapse>
    </div>
  );
};

export default CourseLecture;
