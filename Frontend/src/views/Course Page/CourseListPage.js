import React, { useEffect, useState } from "react";
import HomeLayout from "layouts/Home/HomeLayout";
import * as Actions from 'actions';
import { useDispatch, useSelector } from "react-redux";
import StarRating from "components/StarRating";
import { Link, useLocation } from "react-router-dom";
import qs from 'qs';

const CourseListPage = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const courses = useSelector(({ courses }) => courses.data);
  const topics = useSelector(({ topic }) => topic.data);

  const [title, setTitle] = useState('');

  useEffect(() => {
	let query = qs.parse(location.search, {
		ignoreQueryPrefix: true
	});
	if(query.topic && !Array.isArray(query.topic)){
		query.topic = [query.topic]
	}
	dispatch(Actions.getCoursesList(query));

	return () => {
		dispatch(Actions.clearCoursesListData());
	}
  }, [dispatch, location]);

  useEffect(() => {
    if (courses) {
      let query = qs.parse(location.search, {
        ignoreQueryPrefix: true,
      });
	  if(query.topic && !Array.isArray(query.topic)){
		query.topic = [query.topic]
	  }

      if (query.search) {
        return setTitle(`${courses.length} results for "${query.search}"`);
      }
	  if (query.topic?.length > 0 && topics) {
		let combineName = "";
		query.topic.map((item, index) => {
			const topicData = topics.find(t => t._id === item);
			if(topicData) return combineName += `${index && ','} "${topicData.name}"`;
			return null;
		});
		
		return setTitle(`${courses.length} results for ${combineName}`);
	  }
	  return setTitle(`${courses.length} results`)
    }
  }, [courses, location, topics]);

  return (
    <HomeLayout>
      <div className="my-2 font-bold text-3xl">{title}</div>
	  {courses && (
		<div>
			{courses.map((item, index) => (
				<div className="flex py-6 border-slate-300 border-b-[1px]" key={index}>
					<Link to={`/course/${item.slug}`}>
						<img 
							src={`${process.env.REACT_APP_API_URL}/file/${item.coverImage}`}
							className="mr-4" 
							alt="course thumbnail" 
							width={260}
							height={145}
						/>
					</Link>
					<div>
						<Link to={`/course/${item.slug}`} className="font-bold text-lg mb-[4px] text-black">{item.title}</Link>
						<div className="max-w-[600px] line-clamp-2 mb-[4px]">{item.shortDescription}</div>
						<div className="text-sm mb-[4px] text-slate-500">{item.createdBy?.name}</div>
						<div className="mb-[4px]"><StarRating star={5} /></div>
						<div className="text-sm mb-[4px] text-slate-500">
							{item.sections?.length ?? 0} sections &#x2022; {item.level}
						</div>
					</div>
				</div>
			))}
		</div>
	  )}
    </HomeLayout>
  );
};

export default CourseListPage;
