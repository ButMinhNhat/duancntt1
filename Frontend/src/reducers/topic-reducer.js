import { createSlice } from "@reduxjs/toolkit";
import * as Actions from "actions";

const coursesSlice = createSlice({
    name: 'topic',
    initialState:{
		data: null
	},
    reducers:{},
	extraReducers: (builder) => {
		builder.addCase(Actions.getTopicsList.fulfilled, (state, action) => {
		    state.data = action.payload
		})
	},	
});

const { reducer } = coursesSlice;
export default reducer;