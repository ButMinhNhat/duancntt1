import { Tab, Tabs } from "@mui/material";
import React, { useState } from "react";
import { makeStyles } from "@mui/styles";
import CourseOverview from "./CourseOverview";
import CourseDiscuss from "./CourseDiscuss";

const useStyles = makeStyles({
	tabs: {
		"& .MuiTabs-indicator": {
			backgroundColor: "black",
			height: 4,
		},
		"& .MuiTab-root.Mui-selected": {
			color: "black",
			fontWeight: "bold"
		},
	},
});

const CourseLearnInformation = () => {
	const classes = useStyles();
	const [activeTab, setActiveTab] = useState(0);

	return (
		<div className="mx-4">
			<Tabs
				value={activeTab}
				onChange={(e, value) => setActiveTab(value)}
				aria-label="course information tabs"
				className={classes.tabs}
			>
				<Tab label="Information" className="mx-4 p-4 normal-case text-lg font-bold"/>
				<Tab label="Discuss" className="mx-4 p-4 normal-case text-lg font-bold"/>
			</Tabs>
			<div className="m-4 mb-10 min-h-[300px]">
				{activeTab === 0 && ( <CourseOverview /> )}
				{activeTab === 1 && ( <CourseDiscuss  /> )}
			</div>
		</div>
	);
};

export default CourseLearnInformation;
