import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const TRANSACTION_MUTATION = gql`
  mutation mutationTransaction($input: TransactionInput!) {
    mutationTransaction(input: $input) {
      _id
      type
      course {
        _id
      }
      parent
    }
  }
`;

export const createTransaction = async (input) => {
  try {
    let result = await GraphQLClient.mutate({
      mutation: TRANSACTION_MUTATION,
      variables: { input },
    });

    return {
      data: result.data?.mutationTransaction,
    };
  } catch (error) {
    console.log(error);
    return {
      error: error,
    };
  }
};
