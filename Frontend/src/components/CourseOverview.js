import { Collapse, Typography } from '@mui/material';
import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import UserAvatar from './UserAvatar';

const CourseOverview = () => {
	const [isTruncated, setIsTruncated] = useState(true);

	const course = useSelector(({ course }) => course.data);

	if (!course) return null;

	return (
		<React.Fragment>
			<Collapse in={!isTruncated} collapsedSize={650}>
				<div className="flex py-6 border-b-[1px] border-gray-300 flex-row md:flex-col">
					<div className="flex-1">
						<Typography className="font-semibold my-2 text-2xl">About this course</Typography>
						<Typography className="text-lg">{course.shortDescription}</Typography>
					</div>
				</div>
				<div className="flex py-6 border-b-[1px] border-gray-300 flex-row">
					<div className="text-lg w-full md:w-[230px] font-semibold">Features</div>
					<div className="flex-1 text-lg">
						<div>Skill Level: {course.level}</div>
						<div>Lectures: {course.sections?.length}</div>
					</div>
				</div>
				<div className="flex py-6 border-b-[1px] border-gray-300 flex-row">
					<div className="text-lg w-full md:w-[230px] font-semibold">Description</div>
					<div className="flex-1 text-lg">
						<div dangerouslySetInnerHTML={{ __html: course.description }} />
					</div>
				</div>
				<div className="flex py-6 border-b-[1px] border-gray-300 flex-row">
					<div className="text-lg w-full md:w-[230px] font-semibold">Former</div>
					<div className="flex-1 text-lg flex items-center">
						<UserAvatar url={course?.createdBy?.avatar ?? ""} className="mr-4" />
						<span className="font-semibold text-lg">{course?.createdBy?.name}</span>
					</div>
				</div>
			</Collapse>
			<div
				className="font-semibold text-violet-700 cursor-pointer mt-4"
				onClick={() => setIsTruncated(!isTruncated)}
			>
				{isTruncated ? "Show More" : "Show Less"}
			</div>
		</React.Fragment>
	)
}

export default CourseOverview;