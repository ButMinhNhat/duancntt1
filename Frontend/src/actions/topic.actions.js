import { createAsyncThunk } from "@reduxjs/toolkit";
import * as queries from 'graphql/query';

export const getTopicsList = createAsyncThunk(
	'GET Topics List',
	async () => {
		const result = queries.getTopics();
		return result;
	}
)