import axios from "axios";
import React, { useRef } from "react";

const CustomUpload = (props) => {
  const inputRef = useRef(null);
  const handleClick = () => {
    inputRef.current.click();
  };

  const handleUpload = async (e) => {
    const file = e.target.files[0];
    if (!file) return;
    const acceptFile = props.accept?.split(",") || [];
    if (acceptFile.includes(file.type) || acceptFile.length === 0) {
      let data = new FormData();
      data.append(
        "operations",
        JSON.stringify({
          query: "mutation uploadFile($file:Upload!) {uploadFile(file: $file)}",
        })
      );
      data.append("map", JSON.stringify({ 0: ["variables.file"] }));
      data.append("0", file);
      try {
        axios({
          method: "post",
          url: `${process.env.REACT_APP_API_URL}/graphql`,
          data: data,
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then(function (res) {
            props.handleUpload(res.data?.data?.uploadFile);
          })
          .catch(function (err) {
            console.error(err);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log(`Only accept file ${props.accept}`);
    }
  };

  return (
    <div>
      <div onClick={handleClick}>{props.children}</div>
      <input
        ref={inputRef}
        accept={props.accept ?? ""}
        style={{ display: "none" }}
        type="file"
        onChange={handleUpload}
      />
    </div>
  );
};

export default CustomUpload;
