import React from 'react'

const UserAvatar = (props) => {
  return (
	<img 
		alt="avatar"
		className={`${props.className ?? ''} h-[50px] w-[50px] rounded-full object-scale-down`}
		src={props.url ? `${process.env.REACT_APP_API_URL}/file/${props.url}` : `${process.env.PUBLIC_URL}/user-default.png`}
	/>
  )
}

export default UserAvatar;
