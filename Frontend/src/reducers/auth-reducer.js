import { createSlice } from "@reduxjs/toolkit";
import * as Actions from "actions";

const authSlice = createSlice({
    name: 'auth',
    initialState:{
		data: JSON.parse(localStorage.getItem('user-info')) ?? null,
		token: localStorage.getItem('token') ?? null,
	},
    reducers:{},
	extraReducers: (builder) => {
		builder.addCase(Actions.loginAccount.fulfilled, (state, action) => {
		    localStorage.setItem('user-info', JSON.stringify(action.payload.data))
			localStorage.setItem('token', action.payload.token)

			state.data = action.payload.data
			state.token = action.payload.token
		})
	},	
});

const { reducer } = authSlice;
export default reducer;