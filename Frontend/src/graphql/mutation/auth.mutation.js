import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const LOGIN = gql`
  mutation loginAccount($input: LoginInput!) {
    loginAccount(input: $input) {
      data {
        _id
        name
        username
        password
        avatar
      }
      token
    }
  }
`;

export const login = async (input) => {
  try {
    let result = await GraphQLClient.mutate({
      mutation: LOGIN,
      variables: { input },
    });

    return result.data?.loginAccount;
  } catch (error) {
    console.log(error);
  }
};

const REGISTER = gql`
  mutation registerAccount($input: RegisterInput!) {
    registerAccount(input: $input) {
      data {
        _id
        name
        username
        avatar
      }
      token
    }
  }
`;

export const register = async (input) => {
  try {
    let result = await GraphQLClient.mutate({
      mutation: REGISTER,
      variables: { input },
    });

    return result.data?.registerAccount;
  } catch (error) {
    console.log(error);
  }
};
