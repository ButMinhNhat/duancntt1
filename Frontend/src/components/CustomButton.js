import React from "react";

const CustomButton = (props) => {
  return (
    <button 
		{...props}
		className={`
			text-white bg-indigo-700 hover:bg-indigo-800 
			focus:ring-4 focus:outline-none focus:ring-blue-300 
			font-medium rounded-xl text-sm px-4 py-2 
			dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800
			${props.className ?? ""}
		`}
	>
      {props.children}
    </button>
  );
};

export default CustomButton;
