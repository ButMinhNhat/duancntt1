import { gql } from "@apollo/client/core";
import GraphQLClient from "../config";

const GET_COURSES = gql`
  query GetCouses($search: String, $topics: [String!]) {
    getCourses(search: $search, topics: $topics) {
      _id
      title
      slug
      status
      attendees {
        _id
        name
      }
      sections {
        title
        lectures {
		  _id
          title
          media
          files
          content
          sequence
        }
        quiz {
          _id
          title
          questions {
            type
            title
            answers
            correctAnswers
          }
          sequence
        }
      }
      shortDescription
      description
      coverImage
      level
      hashtags
      features
      topic {
        _id
        name
      }
      createdBy {
        _id
        name
        avatar
      }
    }
  }
`;

export const getCourses = async (query = {}) => {
  try {
    let result = await GraphQLClient.query({
      query: GET_COURSES,
	  variables: query,
      fetchPolicy: "no-cache",
    });

    return result.data?.getCourses ?? [];
  } catch (error) {
    console.log(error);
  }
};

const GET_COURSE_BY_SLUG = gql`
  query GetCouseBySlug($slug: String!) {
    getCourseBySlug(slug: $slug) {
      _id
      title
      slug
      status
      attendees {
        _id
        name
      }
      sections {
        title
        lectures {
		  _id
          title
          media
          files
          content
          sequence
        }
        quiz {
          _id
          title
          questions {
            type
            title
            answers
            correctAnswers
          }
          sequence
        }
      }
      shortDescription
      description
      coverImage
      level
      hashtags
      features
      topic {
        _id
        name
      }
      createdBy {
        _id
        name
        avatar
      }
      createdAt
      updatedAt
    }
  }
`;

export const getCourseBySlug = async (slug) => {
  try {
    let result = await GraphQLClient.query({
      query: GET_COURSE_BY_SLUG,
      variables: { slug },
      fetchPolicy: "no-cache",
    });

    return {
      data: result.data?.getCourseBySlug,
    };
  } catch (error) {
    console.log(error);
    return { error };
  }
};

const GET_INSTRUCTOR_COURSES = gql`
  query GetInstructorCourses {
    getInstructorCourses {
      _id
      title
      slug
      status
      attendees {
        _id
        name
      }
      sections {
        title
        lectures {
		  _id
          title
          media
          files
          content
          sequence
        }
        quiz {
          _id
          title
          questions {
            type
            title
            answers
            correctAnswers
          }
          sequence
        }
      }
      shortDescription
      description
      coverImage
      level
      hashtags
      features
      topic {
        _id
        name
      }
      createdBy {
        _id
        name
        avatar
      }
      createdAt
      updatedAt
    }
  }
`;

export const getInstructorCourses = async () => {
  try {
    let result = await GraphQLClient.query({
      query: GET_INSTRUCTOR_COURSES,
      fetchPolicy: "no-cache",
    });

    return {
      data: result.data?.getInstructorCourses,
    };
  } catch (error) {
    console.log(error);
    return { error };
  }
};

const GET_INSTRUCTOR_COURSE_BY_SLUG = gql`
  query GetInstructorCourseBySlug($slug: String!) {
    getInstructorCourseBySlug(slug: $slug) {
      _id
      title
      slug
      status
      attendees {
        _id
        name
      }
      sections {
        title
        lectures {
		  _id
          title
          media
          files
          content
          sequence
        }
        quiz {
          _id
          title
          questions {
            type
            title
            answers
            correctAnswers
          }
          sequence
        }
      }
      shortDescription
      description
      coverImage
      level
      hashtags
      features
      topic {
        _id
        name
      }
      createdBy {
        _id
        name
        avatar
      }
      createdAt
      updatedAt
    }
  }
`;

export const getInstructorCourseBySlug = async (slug) => {
  try {
    let result = await GraphQLClient.query({
      query: GET_INSTRUCTOR_COURSE_BY_SLUG,
      variables: { slug },
      fetchPolicy: "no-cache",
    });

    return {
      data: result.data?.getInstructorCourseBySlug,
    };
  } catch (error) {
    console.log(error);
    return { error };
  }
};

const GET_MY_LEARNING_COURSES = gql`
  query GetMyLearningCourses {
    getMyLearningCourses {
	  _id
      title
      slug
      status
      attendees {
        _id
        name
      }
      sections {
        title
        lectures {
		  _id
          title
          media
          files
          content
          sequence
        }
        quiz {
          _id
          title
          questions {
            type
            title
            answers
            correctAnswers
          }
          sequence
        }
      }
      shortDescription
      description
      coverImage
      level
      hashtags
      features
      topic {
        _id
        name
      }
      createdBy {
        _id
        name
      }
      createdAt
      updatedAt
    }
  }
`;

export const getMyLearningCourses = async () => {
  try {
    let result = await GraphQLClient.query({
      query: GET_MY_LEARNING_COURSES,
      fetchPolicy: "no-cache",
    });

    return {
      data: result.data?.getMyLearningCourses,
    };
  } catch (error) {
    console.log(error);
    return { error };
  }
};
